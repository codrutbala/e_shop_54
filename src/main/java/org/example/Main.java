package org.example;
/*
Avem un magazin online care vinde Produse (flori si alte aranjamente florale). Doamna Maria vrea să tina evidența la produsele care se aduc in magazin,
la stocul produselor, pretul lor, comenzile care sunt facute de clienti și eventualele pierderi. Mai luăm în considerare vechimea produselor, prețul,
o data de expirare pentru unele dintre ele.
Alte features
* - produse compuse: de exemplu un buchet cu mai multe flori
Produs
- tipul produsului (flori naturale, flori artificiale, felicitari)   <- Enum SAU (tabel tip_produs cu relatie one to many/many to one)
- denumirea <- string
- specificul <- string
- cantitate sau stoc <- integer
- preț achiziție <- double
- preț vânzare <- double
- data achiziției <- LocalDate
- terment de valabiliate <- (o perioadă)
PK(tip, denumire, specificul, data achizitie)
ex: flori naturale, trandafiri, galbeni, 15.05.2023
ex: flori naturale, lalele, olandeze, 15.05.2023
ex: flori artificiale, trandafiri, galbeni, 15.05.2023
Client
- id <- long
- adresa_email
- telefon
- nume <- String
- prenume <- String
- adresa <- String
PK(id)
Comanda
- id/nr comenzii <-long
- client_id <- long
- adresa_livrare
- preț_final
- mod de plată cash/card/rate <- Enum
Rand_comanda
- id
- produs_id
- cantitate
- preț_final (cantitate x preț produs)
- comanda_id

 */

import org.example.config.DatabaseConfig;
import org.example.entity.*;
import org.example.repository.ClientRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.List;

public class Main {
    public static SupplierRepository supplierRepository = new SupplierRepository(DatabaseConfig.getSessionFactory());
    public static ProductRepository productRepository = new ProductRepository(DatabaseConfig.getSessionFactory());
    public static ClientRepository clientRepository = new ClientRepository(DatabaseConfig.getSessionFactory());
    public static ServicesRepository servicesRepository = new ServicesRepository(DatabaseConfig.getSessionFactory());


    public static void main(String[] args) {
        Supplier s1 = new Supplier(1, "Pepiniera Marioara", "Brasov - Romania");
        Supplier s2 = new Supplier(2, "Depozitul de Lalele", "Olanda");

        supplierRepository.save(s1);
        supplierRepository.save(s2);

        saveAllClients();


        Product p1 = new Product(1, ProductType.NATURAL_FLOWERS, "Trandafiri", "Rosii", 11, 280d, 300d, LocalDate.of(2023, 5, 14), LocalDate.of(2023, 6, 13));
        Product p2 = new Product(2, ProductType.NATURAL_FLOWERS, "Trandafiri", "Albastri", 11, 290d, 330d, LocalDate.of(2023, 5, 14), LocalDate.of(2023, 6, 13));

        productRepository.save(p1);
        productRepository.save(p2);


        displayAllClients();
        displayAllProducts();



    }

    public static void displayAllClients() {
        List<Client> clients = clientRepository.getAll();
        for (Client c : clients) {
            System.out.println("Nume: " + c.getLastName() + "Prenume: " + c.getFirstName() + "Email: " + c.getEmail());
        }

    }

    public static void displayAllProducts() {
        List<Product> products = productRepository.getAll();
        for (Product p : products) {
            System.out.println("Nume: " + p.getName() + " descriere: " + p.getDescription() + " bucati: " + p.getQuantity() + " cumparati la data de: " + p.getBuyingDate() +
                    " la pretul de " + p.getBuyingPrice() + " lei " + "valabili pana la " + p.getExpirationDate() + " se vand la pretul de " + p.getSellingPrice() + " lei.");
        }

    }

    public static void saveAllClients() {
        Client c1 = new Client(1, "Ion", "Pop", "ion.pop@gmail.com");
        Client c2 = new Client(2, "Ioana", "Pop", "ioana.pop@gmail.com");
        Client c3 = new Client(3, "Ionuț", "Pop", "ionut.pop@gmail.com");
        clientRepository.save(c1);
        clientRepository.save(c2);
        clientRepository.save(c3);
    }


    public static void saveAllServices() {
        Services service1 = new Services(1, "livrare", 10.2, "Oradea");
        Services service2 = new Services(2, "felicitare", 12.2, "Biharia");
        servicesRepository.save(service1);
        servicesRepository.save(service2);

        SessionFactory sessionFactory=DatabaseConfig.getSessionFactory();
        Session session1 = sessionFactory.openSession();
        Transaction transaction1 = session1.beginTransaction();
        session1.persist(service1);
        session1.persist(service2);
        transaction1.commit();
        session1.close();

        saveAllServices();
    }

}