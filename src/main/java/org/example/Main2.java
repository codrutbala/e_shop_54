package org.example;

import org.example.entity.Rating;
import org.example.entity.UserFeedback;
import org.example.repository.UserFeedbackRepository;

public class Main2 {
    private static UserFeedbackRepository userFeedbackRepository = new UserFeedbackRepository();

    public static void main(String[] args) {
        UserFeedback uf1 = new UserFeedback(1, Rating.GOOD, "Super florarie");
        UserFeedback uf2 = new UserFeedback(2, Rating.GOOD, "Super florarie tare");
        UserFeedback uf3 = new UserFeedback(3, Rating.GOOD, "Super tare");

        userFeedbackRepository.createUserFeedback(uf1);
        userFeedbackRepository.createUserFeedback(uf2);
        userFeedbackRepository.createUserFeedback(uf3);
        System.out.println(uf1.toString());        // User feedback 1 este GOOD: Super florărie

        System.out.println("uf1 a fost salvat in db");


        //update
        uf1.setFeedbackDescription("Ne-a placut foarte mult floraria");
        userFeedbackRepository.updateUserFeedback(uf1);
        System.out.println(userFeedbackRepository.getAllUserFeedbacks());

//delete
        userFeedbackRepository.deleteUserFeedback(uf2);
        System.out.println(userFeedbackRepository.getAllUserFeedbacks());

    }

}