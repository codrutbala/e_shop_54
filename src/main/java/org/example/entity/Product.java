package org.example.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/*
Produs
- tipul produsului (flori naturale, flori artificiale, felicitari)   <- Enum SAU (tabel tip_produs cu relatie one to many/many to one)
- denumirea <- string
- specificul <- string
- cantitate sau stoc <- integer
- preț achiziție <- double
- preț vânzare <- double
- data achiziției <- LocalDate
- terment de valabiliate <- (o perioadă)
PK(tip, denumire, specificul, data achizitie)
ex: flori naturale, trandafiri, galbeni, 15.05.2023
ex: flori naturale, lalele, olandeze, 15.05.2023
ex: flori artificiale, trandafiri, galbeni, 15.05.2023
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    private Integer id;
    @Column(name = "product_type")
    @Enumerated(EnumType.STRING)
    ProductType productType;

    @Column(nullable = false)
    private String name;
    private String description;
    private int quantity;
    @Column(name = "buying_price")
    private Double buyingPrice;
    @Column(name = "selling_price")
    private Double sellingPrice;
    @Column(name = "buying_date")
    private LocalDate buyingDate;
    @Column(name = "selling_date")
    private LocalDate expirationDate;


}
