package org.example.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
Services
- id
- name
- price
- details
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Services {
    public void setId(Integer id) {
        this.id = id;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setServicePrice(Double servicePrice) {
        this.servicePrice = servicePrice;
    }

    public void setServiceDetails(String serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    @Id
    private Integer id;

    @Column (name = "service_name")
    private String serviceName;

    @Column (name = "service_price")
    private Double servicePrice;

    @Column (name = "service_details")
    private String serviceDetails;




}
