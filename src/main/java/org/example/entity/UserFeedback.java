package org.example.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_feedback")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class UserFeedback {

    @Id
    private Integer id;

    @Enumerated(value = EnumType.STRING) // se asigra ca in baza de date se salveaza GOOD sau BAD, nu 1 sau 2
    private Rating rating;

    @Column (name= "Feedback_description")
    private String feedbackDescription;

    //User feedback este GOOD: Super florarie!
    public String toString(){
String s = "User feedback  " + id + " este " + rating +": " + " Super florarie!";
return s;
    }


}
