package org.example.repository;

import org.example.entity.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import java.util.ArrayList;
import java.util.List;

public class ClientRepository {
    private final SessionFactory sessionFactory;

    public ClientRepository(SessionFactory SessionFactory) {
        this.sessionFactory = SessionFactory;
    }

    public void save(Client client) {
        Session session = sessionFactory.openSession();         //deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   //incepe o tranzactie
        session.persist(client);                              // avem o singura modificare: salvarea clientului
        transaction.commit();                                   // salveaza modificarile facute in baza de date dupa deschiderea tranzactiei
        session.close();                                        //inchide sesiunea de comunicare
    }

    public List<Client> getAll() {
        List<Client> clients = new ArrayList<>();
        Session session = sessionFactory.openSession();
        //SELECT * from client - varianta SQL
        //acesta e un query de hibernate, diferit de cel de SQL
        //hql: SELECT c FROM Client c - selecteaza toate randurile din tabelul Client; litera "c" este un alias pt un rand
        clients = session.createQuery("SELECT c FROM Client c", Client.class).getResultList();
        session.close();
        return clients;

    }
}
