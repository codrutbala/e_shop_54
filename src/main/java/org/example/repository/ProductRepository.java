package org.example.repository;
import org.example.entity.Client;
import org.example.entity.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private final SessionFactory sessionFactory;

    public ProductRepository (SessionFactory SessionFactory){
        this.sessionFactory=SessionFactory;
    }
    public void save(Product product) {
        Session session = sessionFactory.openSession();         //deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   //incepe o tranzactie
        session.persist(product);                              // avem o singura modificare: salvarea clientului
        transaction.commit();                                   // salveaza modificarile facute in baza de date dupa deschiderea tranzactiei
        session.close();                                        //inchide sesiunea de comunicare

    }

    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        Session session = sessionFactory.openSession();
        //SELECT * from product - varianta SQL
        //acesta e un query de hibernate, diferit de cel de SQL
        //hql: SELECT p FROM Product p - selecteaza toate randurile din tabelul Product; litera "p" este un alias pt un rand
        products = session.createQuery("SELECT p FROM Product p", Product.class).getResultList();
        session.close();
        return products;
}


}
