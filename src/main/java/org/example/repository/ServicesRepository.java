package org.example.repository;

import org.example.entity.Services;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;


public class ServicesRepository {
    private final SessionFactory sessionFactory;

    public ServicesRepository(SessionFactory SessionFactory){
        this.sessionFactory=SessionFactory;
    }

    public void save(Services services) {
        Session session = sessionFactory.openSession();         //deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   //incepe o tranzactie
        session.persist(services);                              // avem o singura modificare: salvarea clientului
        transaction.commit();                                   // salveaza modificarile facute in baza de date dupa deschiderea tranzactiei
        session.close();                                        //inchide sesiunea de comunicare

    }

    public List<Services> getAll() {
        List<Services> services = new ArrayList<>();
        Session session = sessionFactory.openSession();
        //SELECT * from product - varianta SQL
        //acesta e un query de hibernate, diferit de cel de SQL
        //hql: SELECT p FROM Product p - selecteaza toate randurile din tabelul Product; litera "p" este un alias pt un rand
        services = session.createQuery("SELECT s FROM Services s", Services.class).getResultList();
        session.close();
        return services;
    }



}
