package org.example.repository;

import org.example.entity.Product;
import org.example.entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SupplierRepository {
    private SessionFactory sessionFactory;

    public SupplierRepository(SessionFactory SessionFactory) {
        this.sessionFactory = SessionFactory;
    }

    public void save(Supplier supplier) {
        Session session = sessionFactory.openSession();         //deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();   //incepe o tranzactie
        session.persist(supplier);                              // avem o singura modificare: salvarea supplierului
        transaction.commit();                                   // salveaza modificarile facute in baza de date dupa deschiderea tranzactiei
        session.close();                                        //inchide sesiunea de comunicare


    }

}
